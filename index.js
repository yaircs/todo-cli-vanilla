#!/usr/bin/env node
import fs from 'fs/promises';
import log from '@ajar/marker';


async function save(data={}, file='data.json') {
    // async write file to json
    await fs.writeFile('data.json', JSON.stringify(data, null, 2));
}

async function load(file='data.json') {
    // async load of data, if file is empty return an empty object
    const dirList = await fs.readdir('./');
    let text = '';
    if (dirList.includes('data.json')) text = await fs.readFile(file, 'utf-8');
    
    return JSON.parse(
        text.length ? text : '{}'
        )  
}
    
async function generateId () {
    // find max id and return the next one (+1)
    let state = await load();

    let nextId = 0;
    state.tasks.forEach(task => {
        if (task.id > nextId) nextId = task.id;
    })
    nextId += 1;

    return nextId;
}

async function isIdExsits (id) {
    // checks if input id param is a valid id
    const state = await load();

    if (state.tasks.some(task => task.id === Number(id))) return true;
    else {
        log.red(`Error: Task ID '${id}' not found`);
        return false;
    }
}

function help() {
    log.green('\n');
    log.green('Available commands are:');
    log.cyan('-help')
    for(let action in actions) log.cyan(`-${action}`);
  }

// actions functions object
const actions = {
    add: async function (...title) {
        // load data, generate next id, push item, increment counter and save data
        let state = await load();
        const nextId = await generateId();

        state.tasks.push({
            id: nextId,
            isCompleted: '☐',
            title: title.join(' '),
        });
        state.tasksCounter++;
        save(state);
    },

    delete: async function (id) {
        // load data, check if id is valid, filter data, decrement counter and save data
        let state = await load();
        if (await isIdExsits(id)){ 
            state.tasks = state.tasks.filter(task => task.id !== Number(id))  
            state.tasksCounter = state.tasksCounter-1;
            save(state);  
        }
        else return;
    },

    check: async function (id) {
        // load data, find the right task and change it's isCompleted prop to true and save data
        let state = await load();

        if (await isIdExsits(id)) state.tasks.forEach(task => task.id === Number(id) ? task.isCompleted = '✔': null);
        else return;

        save(state);
    },

    uncheck: async function (id) {
        // load data, find the right task and change it's isCompleted prop to false and save data
        let state = await load();

        if (await isIdExsits(id)) state.tasks.forEach(task => task.id === Number(id) ? task.isCompleted = '☐': null);
        else return;

        save(state);
    },

    update: async function (id, ...newTitle) {
            // load data, find the right task and change it's title prop to new input title and save data
        let state = await load();
        
        if (await isIdExsits(id)) state.tasks.forEach(task => task.id === Number(id) ? task.title = newTitle.join(' ') : null)
        else return;

        save(state);
    },

    list: async function () {
        // load data and print
        const state = await load();
        console.table(state.tasks); 
    }
  
};

// IFFE init
(async function init () {
    // load data, if data is empty create template
    const state = await load();
    if(!state.tasksCounter) await save({tasksCounter: 0, tasks: []});

    // if a param\command has been passed via cli
    if (process.argv[2]) {
        // seperate command and the rest of the args
        const [command, ...args] = [ ...process.argv[2].split(' '), ...process.argv.slice(3) ]
        // fire action with ...args if such command exists
        if (actions[command]) actions[command](...args);
        // if such command dont exsits show help menu
        else help();
        
    } else help(); // if no command has been passed show help menu
 })();
